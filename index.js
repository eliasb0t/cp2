/*
 * Name: Elias Bothell
 * Date: April 21, 2021
 * Section: CSE 154 AC
 *
 * This is the JS to add functionality to index.html.
 * it allows the user to add tasks and days, as well as
 * mark tasks as complete.
 */
"use strict";

let dayCount = 1;

/**
 * Add an item to the todo list
 * @param {Object} listNum the list that the new item will be in
 */
function addListItem(listNum) {

  let list = listNum.target.parentElement.id;

  // remove button temporarily
  let button = listNum.target;
  let buttonId = listNum.target.id;
  button.remove();

  // add message
  let message = document.createElement("p");
  message.innerHTML = 'Type your task and press enter to save...';
  document.getElementById(list).appendChild(message);

  // Create input
  let newTaskInput = document.createElement("input");
  newTaskInput.type = 'text';
  document.getElementById(list).appendChild(newTaskInput);
  newTaskInput.focus();

  // start listening for enter key
  /**
   * Adds an item after it has been typed
   * @param {Object} event the user event
   */
  let addItem = function(event) {
    if (event.keyCode === 13) {
      // Save input
      let newTask = document.createElement("li");
      newTask.innerHTML = newTaskInput.value;
      document.getElementById(list).appendChild(newTask);
      newTask.addEventListener('click', markTaskComplete);

      // Remove temporary stuff
      message.remove();
      newTaskInput.remove();

      // Put add task button back & reset event listener
      let btn = document.createElement('button');
      btn.innerHTML = 'Add Task';
      btn.id = buttonId;
      document.getElementById(list).appendChild(btn);
      document.getElementById(btn.id).addEventListener('click', addListItem);

      // Remove event listener so I don't get clones of stuff
      removeEventListener('keyup', addItem);
    }
  };

  window.addEventListener("keyup", addItem);

}

/**
 * Marks a task as complete.
 * @param {Object} task the task to be marked
 */
function markTaskComplete(task) {
  task.target.innerHTML = task.target.innerHTML.strike();
}

/**
 * Add a day with lists
 * @param {Object} button the button clicked
 */
function addDay(button) {

  let days = button.target.parentElement;

  // remove button
  button.target.remove();

  // Increase dayCount
  dayCount++;

  // Create new day
  let newDay = document.createElement('section');
  newDay.id = 'day-' + dayCount;

  // Create header
  let newDayHeader = document.createElement('h2');
  newDayHeader.innerHTML = 'Day ' + dayCount;
  newDay.appendChild(newDayHeader);

  // Create UL
  let newDayList = document.createElement('ul');
  newDayList.id = 'list-' + dayCount;
  newDay.appendChild(newDayList);

  // Create add task button
  let newDayBtn = document.createElement('button');
  newDayBtn.innerHTML = 'Add Task';
  newDayBtn.id = 'add-day-' + dayCount;
  newDayList.appendChild(newDayBtn);
  newDayBtn.addEventListener('click', addListItem);

  days.appendChild(newDay);

  // create new add day button
  let newAddDayBytton = document.createElement('button');
  newAddDayBytton.innerHTML = 'Add Day';
  newAddDayBytton.id = 'add-day';
  days.appendChild(newAddDayBytton);
  document.getElementById('add-day').addEventListener('click', addDay);
}

/**
 * Initiate the program by adding event listeners
 */
function init() {
  document.getElementById('add-day-1').addEventListener('click', addListItem);
  document.getElementById('add-day').addEventListener('click', addDay);
}

window.addEventListener('load', init);
